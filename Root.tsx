/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { Provider as PaperProvider } from 'react-native-paper';
import { useSelector } from 'react-redux';
import { LoginScreen } from './src/screens';
import { TabbedUI } from './src/tabbed-ui';
import { useCheckToken, useDerivedTheme } from './src/hooks';
import { LoadingPage, Status } from './src/components';
import { IRootState } from './src/store';
import { IUser } from './src/store/reducers';
import { RootStack } from './src/routing';
import { AppContainer } from './AppContainer';

declare const global: { HermesInternal: null | {} };

const mainApp = <RootStack.Screen name="Main" component={TabbedUI} />;
const login = <RootStack.Screen name="Login" component={LoginScreen} />;

const RootApp = () => {
  useCheckToken();
  const user = useSelector<IRootState, IUser>((state) => state.user);
  const theme = useDerivedTheme();

  const page = React.useMemo(() => (user.isAuthenticated ? mainApp : login), [
    user.isAuthenticated,
  ]);

  return (
    <PaperProvider theme={theme}>
      <Status />
      <AppContainer>
        <NavigationContainer theme={theme}>
          {user.isInitialized ? (
            <RootStack.Navigator
              headerMode="none"
              initialRouteName={user.isAuthenticated ? 'Main' : 'Login'}>
              {page}
            </RootStack.Navigator>
          ) : (
            <LoadingPage message="Hang on!" />
          )}
        </NavigationContainer>
      </AppContainer>
    </PaperProvider>
  );
};

export default RootApp;
