import { combineReducers, createStore } from 'redux';
import * as actions from '../src/store/actions';
import * as reducer from '../src/store/reducers';

describe('Store', () => {
  it('should create store', () => {
    const rootReducer = combineReducers({
      user: reducer.userReducer,
      settings: reducer.settingsReducer,
    });
    const store = createStore(rootReducer);
    const state = store.getState();

    expect(state.settings.isDarkMode).toBeFalsy();
  });

  it('should update settings', () => {
    const rootReducer = combineReducers({
      user: reducer.userReducer,
      settings: reducer.settingsReducer,
    });
    const store = createStore(rootReducer);
    const initialState = store.getState();
    store.dispatch(actions.dismissBanner(true));
    store.dispatch(actions.setDarkMode(true));

    const state = store.getState();

    expect(state.settings.isDarkMode).not.toEqual(
      initialState.settings.isDarkMode,
    );
    expect(state.settings.bannerDismissed).not.toEqual(
      initialState.settings.bannerDismissed,
    );
  });
});
