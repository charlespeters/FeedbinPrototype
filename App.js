import * as React from 'react';
import { Provider as ReduxConnector } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import * as Sentry from '@sentry/react-native';

import RootApp from './Root';
import { store, persistor } from './src/store';

Sentry.init({
  dsn:
    'https://35218c99e5874c0ea6499ae787a2ea9e@o503006.ingest.sentry.io/5609765',
});

const App = () => {
  return (
    <ReduxConnector store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <RootApp />
      </PersistGate>
    </ReduxConnector>
  );
};

export default App;
