import * as React from 'react';
import 'react-native-gesture-handler';
import {
  ApolloProvider,
  ApolloClient,
  InMemoryCache,
  createHttpLink,
  NormalizedCacheObject,
} from '@apollo/client';
import { connect } from 'react-redux';
import { setContext } from '@apollo/client/link/context';
import { persistCache } from 'apollo3-cache-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { CACHE_KEY, FEEDBIN_KEY, GRAPHQL_ENDPOINT } from './src/utils';
import { LoadingPage } from './src/components';
import { IRootState } from './src/store';
import { ISettings, IUser } from './src/store/reducers';

interface IAppContainer {}

interface IAppContainerStateProps {
  user: IUser;
  settings: ISettings;
}

interface IAppStateResolved {
  client: ApolloClient<NormalizedCacheObject>;
  loaded: true;
}

interface IAppStateInit {
  client: null;
  loaded: false;
}

type AppState = IAppStateInit | IAppStateResolved;

export class _AppContainer extends React.Component<
  IAppContainer & IAppContainerStateProps,
  AppState
> {
  public readonly state: AppState = {
    client: null,
    loaded: false,
  };

  private url: string = GRAPHQL_ENDPOINT;

  private createClient = async (cache: InMemoryCache) => {
    const httpLink = createHttpLink({
      uri: this.url,
    });

    const authLink = setContext(() => {
      if (this.props.user.token !== null) {
        return {
          headers: {
            Authorization: `Basic ${this.props.user.token}`,
          },
        };
      }

      return AsyncStorage.getItem(FEEDBIN_KEY).then((token) => {
        if (token !== null) {
          return {
            headers: {
              Authorization: `Basic ${token}`,
            },
          };
        }
      });
    });

    const client = new ApolloClient({
      link: authLink.concat(httpLink),
      cache,
      connectToDevTools: __DEV__,
    });

    return client;
  };

  public async componentDidMount() {
    const cache = new InMemoryCache({
      typePolicies: {
        Query: {
          fields: {
            unread: {
              merge: false,
            },
          },
        },
      },
    });
    const client = await this.createClient(cache);
    try {
      await persistCache({
        cache,
        // @ts-ignore
        storage: AsyncStorage,
        key: CACHE_KEY,
      });
    } catch (error) {
      console.error('Error restoring Apollo cache', error);
    }
    this.setState({
      client,
      loaded: true,
    });
  }

  public render(): JSX.Element {
    if (!this.state.loaded) {
      return <LoadingPage message="Loading Cache &amp; Data" />;
    }

    return (
      <ApolloProvider client={this.state.client}>
        {this.props.children}
      </ApolloProvider>
    );
  }
}
const mapStateToProps = (state: IRootState): IAppContainerStateProps => {
  return {
    user: state.user,
    settings: state.settings,
  };
};

export const AppContainer = connect<
  IAppContainerStateProps,
  AppState,
  IAppContainer,
  IRootState
>(mapStateToProps)(_AppContainer);
