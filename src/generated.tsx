import { gql } from '@apollo/client';
import * as Apollo from '@apollo/client';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
};

export type IUnreadList = {
  __typename?: 'UnreadList';
  itemIDs: Array<Scalars['String']>;
};

export type ISubscriptions = {
  __typename?: 'Subscriptions';
  tags: Array<ITag>;
  untaggedFeeds: Array<IFeed>;
};

export type IItem = {
  __typename?: 'Item';
  id: Scalars['Float'];
  feed_id: Scalars['Int'];
  title: Scalars['String'];
  author: Maybe<Scalars['String']>;
  summary: Maybe<Scalars['String']>;
  content: Scalars['String'];
  url: Scalars['String'];
  extracted_content_url: Scalars['String'];
  published: Scalars['String'];
  created_at: Maybe<Scalars['String']>;
};

export type ITag = {
  __typename?: 'Tag';
  title: Scalars['String'];
  feeds: Array<Maybe<IFeed>>;
};

export type IFeed = {
  __typename?: 'Feed';
  url: Scalars['String'];
  site: Scalars['String'];
  name: Scalars['String'];
  id: Scalars['ID'];
  feed_id: Scalars['Float'];
};

export type IAuthResponse = {
  __typename?: 'AuthResponse';
  isValid: Scalars['Boolean'];
};

export type ISubscription = {
  __typename?: 'Subscription';
  feed: IFeed;
  items: Array<IItem>;
};

export type IQuery = {
  __typename?: 'Query';
  subscriptions: ISubscriptions;
  favorites: Array<Scalars['Float']>;
  unread: Array<Scalars['Float']>;
  entries: Array<IItem>;
  entry: IItem;
  /** Deprecated */
  subscription: ISubscription;
  bookmarks: Array<IItem>;
  /** Must be id not feed_id */
  feed: IFeed;
};

export type IQueryEntriesArgs = {
  page: Maybe<Scalars['Int']>;
};

export type IQueryEntryArgs = {
  id: Scalars['Float'];
};

export type IQuerySubscriptionArgs = {
  id: Scalars['Float'];
};

export type IQueryBookmarksArgs = {
  ids: Maybe<Array<Scalars['Float']>>;
};

export type IQueryFeedArgs = {
  id: Scalars['Float'];
};

export type IMutation = {
  __typename?: 'Mutation';
  bookmark: Maybe<IItem>;
  removeBookmark: Maybe<IItem>;
  markAsRead: Maybe<IItem>;
  markAsUnread: Maybe<IItem>;
  login: Maybe<IAuthResponse>;
};

export type IMutationBookmarkArgs = {
  id: Scalars['Float'];
};

export type IMutationRemoveBookmarkArgs = {
  id: Scalars['Float'];
};

export type IMutationMarkAsReadArgs = {
  id: Scalars['String'];
};

export type IMutationMarkAsUnreadArgs = {
  id: Scalars['String'];
};

export type IMutationLoginArgs = {
  hash: Scalars['String'];
};

export type IMetaFragment = { __typename?: 'Query' } & Pick<
  IQuery,
  'favorites' | 'unread'
>;

export type IFeedInfoFragment = { __typename?: 'Feed' } & Pick<
  IFeed,
  'name' | 'id' | 'feed_id' | 'url' | 'site'
>;

export type IInitialEntryMetaQueryVariables = Exact<{ [key: string]: never }>;

export type IInitialEntryMetaQuery = { __typename?: 'Query' } & IMetaFragment;

export type IAllSubscriptionsQueryVariables = Exact<{ [key: string]: never }>;

export type IAllSubscriptionsQuery = { __typename?: 'Query' } & {
  subscriptions: { __typename?: 'Subscriptions' } & {
    tags: Array<
      { __typename?: 'Tag' } & Pick<ITag, 'title'> & {
          feeds: Array<Maybe<{ __typename?: 'Feed' } & IFeedInfoFragment>>;
        }
    >;
    untaggedFeeds: Array<{ __typename?: 'Feed' } & IFeedInfoFragment>;
  };
};

export type IAllUnreadQueryVariables = Exact<{
  page: Maybe<Scalars['Int']>;
}>;

export type IAllUnreadQuery = { __typename?: 'Query' } & {
  entries: Array<
    { __typename?: 'Item' } & Pick<
      IItem,
      'title' | 'id' | 'summary' | 'feed_id' | 'published'
    >
  >;
  subscriptions: { __typename?: 'Subscriptions' } & {
    tags: Array<
      { __typename?: 'Tag' } & Pick<ITag, 'title'> & {
          feeds: Array<Maybe<{ __typename?: 'Feed' } & IFeedInfoFragment>>;
        }
    >;
    untaggedFeeds: Array<{ __typename?: 'Feed' } & IFeedInfoFragment>;
  };
  bookmarks: Array<
    { __typename?: 'Item' } & Pick<
      IItem,
      'title' | 'id' | 'feed_id' | 'summary' | 'url' | 'published'
    >
  >;
} & IMetaFragment;

export type IFeedwithEntriesQueryVariables = Exact<{
  id: Scalars['Float'];
}>;

export type IFeedwithEntriesQuery = { __typename?: 'Query' } & {
  subscription: { __typename?: 'Subscription' } & {
    feed: { __typename?: 'Feed' } & Pick<IFeed, 'name'>;
    items: Array<
      { __typename?: 'Item' } & Pick<
        IItem,
        'title' | 'id' | 'feed_id' | 'summary' | 'published'
      >
    >;
  };
};

export type IEntryDocumentQueryVariables = Exact<{
  id: Scalars['Float'];
}>;

export type IEntryDocumentQuery = { __typename?: 'Query' } & {
  entry: { __typename?: 'Item' } & Pick<
    IItem,
    'title' | 'id' | 'feed_id' | 'content' | 'published' | 'url'
  >;
};

export type IGetAllBookmarksQueryVariables = Exact<{ [key: string]: never }>;

export type IGetAllBookmarksQuery = { __typename?: 'Query' } & Pick<
  IQuery,
  'favorites'
> & {
    bookmarks: Array<
      { __typename?: 'Item' } & Pick<
        IItem,
        'title' | 'id' | 'feed_id' | 'summary' | 'url' | 'published'
      >
    >;
  };

export type IFeedbinLoginMutationVariables = Exact<{
  hash: Scalars['String'];
}>;

export type IFeedbinLoginMutation = { __typename?: 'Mutation' } & {
  login: Maybe<
    { __typename?: 'AuthResponse' } & Pick<IAuthResponse, 'isValid'>
  >;
};

export type IMarkAsReadMutationVariables = Exact<{
  id: Scalars['String'];
}>;

export type IMarkAsReadMutation = { __typename?: 'Mutation' } & {
  markAsRead: Maybe<{ __typename?: 'Item' } & Pick<IItem, 'id' | 'title'>>;
};

export type IBookmarkEntryMutationVariables = Exact<{
  id: Scalars['Float'];
}>;

export type IBookmarkEntryMutation = { __typename?: 'Mutation' } & {
  bookmark: Maybe<{ __typename?: 'Item' } & Pick<IItem, 'id'>>;
};

export type IRemoveBookmarkEntryMutationVariables = Exact<{
  id: Scalars['Float'];
}>;

export type IRemoveBookmarkEntryMutation = { __typename?: 'Mutation' } & {
  removeBookmark: Maybe<{ __typename?: 'Item' } & Pick<IItem, 'id'>>;
};

export const MetaFragmentDoc = gql`
  fragment Meta on Query {
    favorites
    unread
  }
`;
export const FeedInfoFragmentDoc = gql`
  fragment FeedInfo on Feed {
    name
    id
    feed_id
    url
    site
  }
`;
export const InitialEntryMetaDocument = gql`
  query InitialEntryMeta {
    ...Meta
  }
  ${MetaFragmentDoc}
`;

/**
 * __useInitialEntryMetaQuery__
 *
 * To run a query within a React component, call `useInitialEntryMetaQuery` and pass it any options that fit your needs.
 * When your component renders, `useInitialEntryMetaQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useInitialEntryMetaQuery({
 *   variables: {
 *   },
 * });
 */
export function useInitialEntryMetaQuery(
  baseOptions?: Apollo.QueryHookOptions<
    IInitialEntryMetaQuery,
    IInitialEntryMetaQueryVariables
  >,
) {
  return Apollo.useQuery<
    IInitialEntryMetaQuery,
    IInitialEntryMetaQueryVariables
  >(InitialEntryMetaDocument, baseOptions);
}
export function useInitialEntryMetaLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    IInitialEntryMetaQuery,
    IInitialEntryMetaQueryVariables
  >,
) {
  return Apollo.useLazyQuery<
    IInitialEntryMetaQuery,
    IInitialEntryMetaQueryVariables
  >(InitialEntryMetaDocument, baseOptions);
}
export type InitialEntryMetaQueryHookResult = ReturnType<
  typeof useInitialEntryMetaQuery
>;
export type InitialEntryMetaLazyQueryHookResult = ReturnType<
  typeof useInitialEntryMetaLazyQuery
>;
export type InitialEntryMetaQueryResult = Apollo.QueryResult<
  IInitialEntryMetaQuery,
  IInitialEntryMetaQueryVariables
>;
export const AllSubscriptionsDocument = gql`
  query AllSubscriptions {
    subscriptions {
      tags {
        title
        feeds {
          ...FeedInfo
        }
      }
      untaggedFeeds {
        ...FeedInfo
      }
    }
  }
  ${FeedInfoFragmentDoc}
`;

/**
 * __useAllSubscriptionsQuery__
 *
 * To run a query within a React component, call `useAllSubscriptionsQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllSubscriptionsQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllSubscriptionsQuery({
 *   variables: {
 *   },
 * });
 */
export function useAllSubscriptionsQuery(
  baseOptions?: Apollo.QueryHookOptions<
    IAllSubscriptionsQuery,
    IAllSubscriptionsQueryVariables
  >,
) {
  return Apollo.useQuery<
    IAllSubscriptionsQuery,
    IAllSubscriptionsQueryVariables
  >(AllSubscriptionsDocument, baseOptions);
}
export function useAllSubscriptionsLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    IAllSubscriptionsQuery,
    IAllSubscriptionsQueryVariables
  >,
) {
  return Apollo.useLazyQuery<
    IAllSubscriptionsQuery,
    IAllSubscriptionsQueryVariables
  >(AllSubscriptionsDocument, baseOptions);
}
export type AllSubscriptionsQueryHookResult = ReturnType<
  typeof useAllSubscriptionsQuery
>;
export type AllSubscriptionsLazyQueryHookResult = ReturnType<
  typeof useAllSubscriptionsLazyQuery
>;
export type AllSubscriptionsQueryResult = Apollo.QueryResult<
  IAllSubscriptionsQuery,
  IAllSubscriptionsQueryVariables
>;
export const AllUnreadDocument = gql`
  query AllUnread($page: Int) {
    ...Meta
    entries(page: $page) {
      title
      id
      summary
      feed_id
      published
    }
    subscriptions {
      tags {
        title
        feeds {
          ...FeedInfo
        }
      }
      untaggedFeeds {
        ...FeedInfo
      }
    }
    bookmarks {
      title
      id
      feed_id
      summary
      url
      published
    }
  }
  ${MetaFragmentDoc}
  ${FeedInfoFragmentDoc}
`;

/**
 * __useAllUnreadQuery__
 *
 * To run a query within a React component, call `useAllUnreadQuery` and pass it any options that fit your needs.
 * When your component renders, `useAllUnreadQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useAllUnreadQuery({
 *   variables: {
 *      page: // value for 'page'
 *   },
 * });
 */
export function useAllUnreadQuery(
  baseOptions?: Apollo.QueryHookOptions<
    IAllUnreadQuery,
    IAllUnreadQueryVariables
  >,
) {
  return Apollo.useQuery<IAllUnreadQuery, IAllUnreadQueryVariables>(
    AllUnreadDocument,
    baseOptions,
  );
}
export function useAllUnreadLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    IAllUnreadQuery,
    IAllUnreadQueryVariables
  >,
) {
  return Apollo.useLazyQuery<IAllUnreadQuery, IAllUnreadQueryVariables>(
    AllUnreadDocument,
    baseOptions,
  );
}
export type AllUnreadQueryHookResult = ReturnType<typeof useAllUnreadQuery>;
export type AllUnreadLazyQueryHookResult = ReturnType<
  typeof useAllUnreadLazyQuery
>;
export type AllUnreadQueryResult = Apollo.QueryResult<
  IAllUnreadQuery,
  IAllUnreadQueryVariables
>;
export const FeedwithEntriesDocument = gql`
  query FeedwithEntries($id: Float!) {
    subscription(id: $id) {
      feed {
        name
      }
      items {
        title
        id
        feed_id
        summary
        published
      }
    }
  }
`;

/**
 * __useFeedwithEntriesQuery__
 *
 * To run a query within a React component, call `useFeedwithEntriesQuery` and pass it any options that fit your needs.
 * When your component renders, `useFeedwithEntriesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useFeedwithEntriesQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useFeedwithEntriesQuery(
  baseOptions?: Apollo.QueryHookOptions<
    IFeedwithEntriesQuery,
    IFeedwithEntriesQueryVariables
  >,
) {
  return Apollo.useQuery<IFeedwithEntriesQuery, IFeedwithEntriesQueryVariables>(
    FeedwithEntriesDocument,
    baseOptions,
  );
}
export function useFeedwithEntriesLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    IFeedwithEntriesQuery,
    IFeedwithEntriesQueryVariables
  >,
) {
  return Apollo.useLazyQuery<
    IFeedwithEntriesQuery,
    IFeedwithEntriesQueryVariables
  >(FeedwithEntriesDocument, baseOptions);
}
export type FeedwithEntriesQueryHookResult = ReturnType<
  typeof useFeedwithEntriesQuery
>;
export type FeedwithEntriesLazyQueryHookResult = ReturnType<
  typeof useFeedwithEntriesLazyQuery
>;
export type FeedwithEntriesQueryResult = Apollo.QueryResult<
  IFeedwithEntriesQuery,
  IFeedwithEntriesQueryVariables
>;
export const EntryDocumentDocument = gql`
  query EntryDocument($id: Float!) {
    entry(id: $id) {
      title
      id
      feed_id
      content
      published
      url
    }
  }
`;

/**
 * __useEntryDocumentQuery__
 *
 * To run a query within a React component, call `useEntryDocumentQuery` and pass it any options that fit your needs.
 * When your component renders, `useEntryDocumentQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useEntryDocumentQuery({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useEntryDocumentQuery(
  baseOptions?: Apollo.QueryHookOptions<
    IEntryDocumentQuery,
    IEntryDocumentQueryVariables
  >,
) {
  return Apollo.useQuery<IEntryDocumentQuery, IEntryDocumentQueryVariables>(
    EntryDocumentDocument,
    baseOptions,
  );
}
export function useEntryDocumentLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    IEntryDocumentQuery,
    IEntryDocumentQueryVariables
  >,
) {
  return Apollo.useLazyQuery<IEntryDocumentQuery, IEntryDocumentQueryVariables>(
    EntryDocumentDocument,
    baseOptions,
  );
}
export type EntryDocumentQueryHookResult = ReturnType<
  typeof useEntryDocumentQuery
>;
export type EntryDocumentLazyQueryHookResult = ReturnType<
  typeof useEntryDocumentLazyQuery
>;
export type EntryDocumentQueryResult = Apollo.QueryResult<
  IEntryDocumentQuery,
  IEntryDocumentQueryVariables
>;
export const GetAllBookmarksDocument = gql`
  query GetAllBookmarks {
    favorites
    bookmarks {
      title
      id
      feed_id
      summary
      url
      published
    }
  }
`;

/**
 * __useGetAllBookmarksQuery__
 *
 * To run a query within a React component, call `useGetAllBookmarksQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAllBookmarksQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetAllBookmarksQuery({
 *   variables: {
 *   },
 * });
 */
export function useGetAllBookmarksQuery(
  baseOptions?: Apollo.QueryHookOptions<
    IGetAllBookmarksQuery,
    IGetAllBookmarksQueryVariables
  >,
) {
  return Apollo.useQuery<IGetAllBookmarksQuery, IGetAllBookmarksQueryVariables>(
    GetAllBookmarksDocument,
    baseOptions,
  );
}
export function useGetAllBookmarksLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    IGetAllBookmarksQuery,
    IGetAllBookmarksQueryVariables
  >,
) {
  return Apollo.useLazyQuery<
    IGetAllBookmarksQuery,
    IGetAllBookmarksQueryVariables
  >(GetAllBookmarksDocument, baseOptions);
}
export type GetAllBookmarksQueryHookResult = ReturnType<
  typeof useGetAllBookmarksQuery
>;
export type GetAllBookmarksLazyQueryHookResult = ReturnType<
  typeof useGetAllBookmarksLazyQuery
>;
export type GetAllBookmarksQueryResult = Apollo.QueryResult<
  IGetAllBookmarksQuery,
  IGetAllBookmarksQueryVariables
>;
export const FeedbinLoginDocument = gql`
  mutation FeedbinLogin($hash: String!) {
    login(hash: $hash) {
      isValid
    }
  }
`;
export type IFeedbinLoginMutationFn = Apollo.MutationFunction<
  IFeedbinLoginMutation,
  IFeedbinLoginMutationVariables
>;

/**
 * __useFeedbinLoginMutation__
 *
 * To run a mutation, you first call `useFeedbinLoginMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useFeedbinLoginMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [feedbinLoginMutation, { data, loading, error }] = useFeedbinLoginMutation({
 *   variables: {
 *      hash: // value for 'hash'
 *   },
 * });
 */
export function useFeedbinLoginMutation(
  baseOptions?: Apollo.MutationHookOptions<
    IFeedbinLoginMutation,
    IFeedbinLoginMutationVariables
  >,
) {
  return Apollo.useMutation<
    IFeedbinLoginMutation,
    IFeedbinLoginMutationVariables
  >(FeedbinLoginDocument, baseOptions);
}
export type FeedbinLoginMutationHookResult = ReturnType<
  typeof useFeedbinLoginMutation
>;
export type FeedbinLoginMutationResult = Apollo.MutationResult<
  IFeedbinLoginMutation
>;
export type FeedbinLoginMutationOptions = Apollo.BaseMutationOptions<
  IFeedbinLoginMutation,
  IFeedbinLoginMutationVariables
>;
export const MarkAsReadDocument = gql`
  mutation MarkAsRead($id: String!) {
    markAsRead(id: $id) {
      id
      title
    }
  }
`;
export type IMarkAsReadMutationFn = Apollo.MutationFunction<
  IMarkAsReadMutation,
  IMarkAsReadMutationVariables
>;

/**
 * __useMarkAsReadMutation__
 *
 * To run a mutation, you first call `useMarkAsReadMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useMarkAsReadMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [markAsReadMutation, { data, loading, error }] = useMarkAsReadMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useMarkAsReadMutation(
  baseOptions?: Apollo.MutationHookOptions<
    IMarkAsReadMutation,
    IMarkAsReadMutationVariables
  >,
) {
  return Apollo.useMutation<IMarkAsReadMutation, IMarkAsReadMutationVariables>(
    MarkAsReadDocument,
    baseOptions,
  );
}
export type MarkAsReadMutationHookResult = ReturnType<
  typeof useMarkAsReadMutation
>;
export type MarkAsReadMutationResult = Apollo.MutationResult<
  IMarkAsReadMutation
>;
export type MarkAsReadMutationOptions = Apollo.BaseMutationOptions<
  IMarkAsReadMutation,
  IMarkAsReadMutationVariables
>;
export const BookmarkEntryDocument = gql`
  mutation BookmarkEntry($id: Float!) {
    bookmark(id: $id) {
      id
    }
  }
`;
export type IBookmarkEntryMutationFn = Apollo.MutationFunction<
  IBookmarkEntryMutation,
  IBookmarkEntryMutationVariables
>;

/**
 * __useBookmarkEntryMutation__
 *
 * To run a mutation, you first call `useBookmarkEntryMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useBookmarkEntryMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [bookmarkEntryMutation, { data, loading, error }] = useBookmarkEntryMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useBookmarkEntryMutation(
  baseOptions?: Apollo.MutationHookOptions<
    IBookmarkEntryMutation,
    IBookmarkEntryMutationVariables
  >,
) {
  return Apollo.useMutation<
    IBookmarkEntryMutation,
    IBookmarkEntryMutationVariables
  >(BookmarkEntryDocument, baseOptions);
}
export type BookmarkEntryMutationHookResult = ReturnType<
  typeof useBookmarkEntryMutation
>;
export type BookmarkEntryMutationResult = Apollo.MutationResult<
  IBookmarkEntryMutation
>;
export type BookmarkEntryMutationOptions = Apollo.BaseMutationOptions<
  IBookmarkEntryMutation,
  IBookmarkEntryMutationVariables
>;
export const RemoveBookmarkEntryDocument = gql`
  mutation RemoveBookmarkEntry($id: Float!) {
    removeBookmark(id: $id) {
      id
    }
  }
`;
export type IRemoveBookmarkEntryMutationFn = Apollo.MutationFunction<
  IRemoveBookmarkEntryMutation,
  IRemoveBookmarkEntryMutationVariables
>;

/**
 * __useRemoveBookmarkEntryMutation__
 *
 * To run a mutation, you first call `useRemoveBookmarkEntryMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useRemoveBookmarkEntryMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [removeBookmarkEntryMutation, { data, loading, error }] = useRemoveBookmarkEntryMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useRemoveBookmarkEntryMutation(
  baseOptions?: Apollo.MutationHookOptions<
    IRemoveBookmarkEntryMutation,
    IRemoveBookmarkEntryMutationVariables
  >,
) {
  return Apollo.useMutation<
    IRemoveBookmarkEntryMutation,
    IRemoveBookmarkEntryMutationVariables
  >(RemoveBookmarkEntryDocument, baseOptions);
}
export type RemoveBookmarkEntryMutationHookResult = ReturnType<
  typeof useRemoveBookmarkEntryMutation
>;
export type RemoveBookmarkEntryMutationResult = Apollo.MutationResult<
  IRemoveBookmarkEntryMutation
>;
export type RemoveBookmarkEntryMutationOptions = Apollo.BaseMutationOptions<
  IRemoveBookmarkEntryMutation,
  IRemoveBookmarkEntryMutationVariables
>;
