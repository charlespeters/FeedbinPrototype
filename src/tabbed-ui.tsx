import * as React from 'react';
import { useTheme } from 'react-native-paper';
import {
  createStackNavigator,
  StackNavigationOptions,
} from '@react-navigation/stack';
import { ShareButton, tabOptions as options } from './components';
import { TabsUI } from './routing';
import {
  UnreadScreen,
  ArticleScreen,
  FeedListScreen,
  BookmarksScreen,
  RecommendedScreen,
  SettingsScreen,
  FeedDetails,
} from './screens';

// Entries --- Stack: Unread, All, Recently Read, Article View
// Feeds --- Stack: Feed List, Feed Details, Article View
// Bookmarks --- Stack: Bookmark Feed, Article View
// Recommended Feeds --- No Stack
//

const createArticleHeader = (
  theme: ReactNativePaper.Theme,
): StackNavigationOptions => ({
  headerTitleStyle: {
    ...theme.fonts.medium,
  },
  headerTintColor: theme.colors.text,
  headerRight: (props) => <ShareButton onPress={() => console.log(props)} />,
});

const UnreadStack = createStackNavigator();

const Unread = () => {
  const theme = useTheme();
  const stackOpts = createArticleHeader(theme);
  return (
    <UnreadStack.Navigator screenOptions={{}}>
      <UnreadStack.Screen
        name="UnreadFeed"
        component={UnreadScreen}
        options={{
          headerTitleStyle: {
            ...theme.fonts.medium,
          },
        }}
      />
      <UnreadStack.Screen
        name="Article"
        component={ArticleScreen}
        options={stackOpts}
      />
    </UnreadStack.Navigator>
  );
};

const FeedStack = createStackNavigator();

const Feeds = () => {
  const theme = useTheme();
  const headerOptions = createArticleHeader(theme);

  return (
    <FeedStack.Navigator>
      <FeedStack.Screen
        name="Subscriptions"
        component={FeedListScreen}
        options={{
          headerTitleStyle: {
            ...theme.fonts.medium,
          },
        }}
      />
      <FeedStack.Screen
        name="SubscriptionDetails"
        component={FeedDetails}
        options={{
          headerTitleStyle: {
            ...theme.fonts.medium,
          },
        }}
      />
      <FeedStack.Screen
        name="Article"
        component={ArticleScreen}
        options={headerOptions}
      />
    </FeedStack.Navigator>
  );
};

const BookmarkStack = createStackNavigator();

const Bookmarks = () => {
  const theme = useTheme();
  const screenOptions = createArticleHeader(theme);

  return (
    <BookmarkStack.Navigator>
      <BookmarkStack.Screen name="Bookmarks" component={BookmarksScreen} />
      <BookmarkStack.Screen
        name="Article"
        component={ArticleScreen}
        options={screenOptions}
      />
    </BookmarkStack.Navigator>
  );
};

const SettingsStack = createStackNavigator();

const Settings = () => {
  return (
    <SettingsStack.Navigator>
      <SettingsStack.Screen name="Settings" component={SettingsScreen} />
    </SettingsStack.Navigator>
  );
};

export const TabbedUI = () => {
  return (
    <TabsUI.Navigator
      tabBarOptions={{
        showLabel: false,
        // activeBackgroundColor: Colors.grey900,
        // inactiveBackgroundColor: Colors.grey900,
        // activeTintColor: Colors.amber600,
      }}>
      <TabsUI.Screen
        name="Unread"
        component={Unread}
        options={options.unread}
      />
      <TabsUI.Screen name="Feed" component={Feeds} options={options.feed} />
      <TabsUI.Screen
        name="Bookmarks"
        component={Bookmarks}
        options={options.bookmarks}
      />
      <TabsUI.Screen
        name="Recommended"
        component={RecommendedScreen}
        options={options.recommend}
      />
      <TabsUI.Screen
        name="Settings"
        component={Settings}
        options={options.settings}
      />
    </TabsUI.Navigator>
  );
};
