import { IAllUnreadQuery, IItem } from './generated';

export const FEEDBIN_KEY = '@feedbin_authorization';
export const CACHE_KEY = '@feedbin_cache';
export const STORE_KEY = '@feedbin_key';
export const GRAPHQL_ENDPOINT = __DEV__
  ? 'http://192.168.86.143:3000/api/graphql'
  : 'https://pre-alpha.reubin.app/api/graphql';

interface IKeyItem {
  id: number;
}
export const itemKeyExtractor = (item: IKeyItem) => item.id.toString();

type EntryCompare = {
  __typename?: 'Item' | undefined;
} & Pick<IItem, 'title' | 'id' | 'summary' | 'feed_id' | 'published'>;

export const sortByDate = (a: EntryCompare, b: EntryCompare) => {
  const aTime = new Date(a.published);
  const bTime = new Date(b.published);

  return bTime.getTime() - aTime.getTime();
};

export enum Filters {
  ALL = 'ALL',
  UNREAD = 'UNREAD',
}

export function getSortedEntries(
  data: IAllUnreadQuery | undefined,
  activeFilter: Filters,
) {
  if (data) {
    const clone = Object.assign({}, data);
    switch (activeFilter) {
      case Filters.ALL:
        return clone.entries.sort(sortByDate);
      case Filters.UNREAD: {
        const s = new Set(clone.unread);

        return clone.entries.filter((e) => s.has(e.id));
      }
      default:
        return clone.entries;
    }
  }

  return data;
}
