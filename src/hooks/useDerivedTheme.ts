import { useMemo } from 'react';
import { useColorScheme } from 'react-native';
import { useSelector } from 'react-redux';
import { lightTheme, darkTheme } from '../theme';
import { IRootState } from '../store';
import { ISettings } from '../store/reducers';

export const useDerivedTheme = () => {
  const systemColorScheme = useColorScheme();
  const settings = useSelector<IRootState, ISettings>(
    (state) => state.settings,
  );

  return useMemo(() => {
    if (settings.useSystemDefault) {
      return systemColorScheme === 'dark' ? darkTheme : lightTheme;
    } else {
      return settings.isDarkMode ? darkTheme : lightTheme;
    }
  }, [systemColorScheme, settings.useSystemDefault, settings.isDarkMode]);
};
