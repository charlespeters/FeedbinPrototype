import { useMemo } from 'react';
import { useApolloClient } from '@apollo/client';
import {
  AllSubscriptionsDocument,
  IAllSubscriptionsQuery,
  IFeedInfoFragment,
  Maybe,
} from '../generated';

type FeedReduced = Array<Maybe<{ __typename?: 'Feed' } & IFeedInfoFragment>>;

export const useReducedFeeds = () => {
  const client = useApolloClient();
  return useMemo(() => {
    const cache = client.readQuery<IAllSubscriptionsQuery>({
      query: AllSubscriptionsDocument,
    });
    return cache?.subscriptions.tags.reduce<FeedReduced>((acc, tag) => {
      return acc.concat(tag.feeds);
    }, cache.subscriptions.untaggedFeeds);
  }, [client]);
};
