import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import * as actions from '../store/actions';

export const useCheckToken = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(actions.checkTokenAsync());
  }, [dispatch]);
};
