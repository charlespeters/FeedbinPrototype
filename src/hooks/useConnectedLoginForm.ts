import { useRef } from 'react';
import { useDispatch } from 'react-redux';
import { useFormik } from 'formik';
import base64 from 'base-64';
import { useFeedbinLoginMutation } from '../generated';
import * as actions from '../store/actions';

interface ILoginFormValues {
  username: string;
  password: string;
}

function getInitialValues(): ILoginFormValues {
  if (__DEV__) {
    return require('../../secrets.json');
  }

  return {
    username: '',
    password: '',
  };
}

export const useConnectedLoginForm = () => {
  const initialValues = useRef(getInitialValues());
  const [mutationFn] = useFeedbinLoginMutation();
  const dispatch = useDispatch();
  const formik = useFormik<ILoginFormValues>({
    initialValues: initialValues.current,
    onSubmit: async (values) => {
      try {
        const hash = base64.encode(`${values.username}:${values.password}`);
        const { data } = await mutationFn({ variables: { hash } });

        if (data?.login?.isValid) {
          dispatch(actions.loginAsync(hash));
        }
      } catch (error) {
        console.log(error);
      }
    },
  });

  return formik;
};
