import { useCallback } from 'react';
import { useApolloClient } from '@apollo/client';
import { GetAllBookmarksDocument, IGetAllBookmarksQuery } from '../generated';

export const useCheckBookmark = () => {
  const client = useApolloClient();

  return useCallback(
    (id) => {
      const cache = client.readQuery<IGetAllBookmarksQuery>({
        query: GetAllBookmarksDocument,
      });

      const isFavorite = id ? cache?.favorites?.includes(id) : false;

      return isFavorite;
    },
    [client],
  );
};
