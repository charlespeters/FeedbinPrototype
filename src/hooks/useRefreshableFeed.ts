import { useState, useCallback, useRef } from 'react';
import { useAllUnreadQuery } from '../generated';

export const useRefreshableFeed = () => {
  const { loading, data, error, refetch } = useAllUnreadQuery();
  const page = useRef<number>(1);

  const [refreshing, setRefreshing] = useState(false);
  const onRefresh = useCallback(async () => {
    if (refetch) {
      setRefreshing(true);
      try {
        await refetch().then(() => setRefreshing(false));
      } catch (err) {
        console.log(err);
      }
    }
  }, [refetch, setRefreshing]);

  const onEndReached = useCallback(async () => {
    if (refetch) {
      try {
        page.current++;
        await refetch({
          page: page.current,
        });
      } catch (err) {
        console.log(err);
      }
    }
  }, [refetch]);

  return [
    {
      loading,
      data,
      error,
      refreshing,
    },
    { onRefresh, onEndReached },
  ] as const;
};
