import { useMemo, useCallback } from 'react';
import { MutationUpdaterFn, useApolloClient } from '@apollo/client';
import {
  IBookmarkEntryMutation,
  IGetAllBookmarksQuery,
  IRemoveBookmarkEntryMutation,
  GetAllBookmarksDocument,
  useBookmarkEntryMutation,
  useRemoveBookmarkEntryMutation,
} from '../generated';

const updateFavorite: MutationUpdaterFn<IBookmarkEntryMutation> = (
  cache,
  { data },
) => {
  if (data) {
    const bookmarks = cache.readQuery<IGetAllBookmarksQuery>({
      query: GetAllBookmarksDocument,
    })!;
    cache.writeQuery<IGetAllBookmarksQuery>({
      query: GetAllBookmarksDocument,
      data: {
        ...bookmarks,
        favorites: bookmarks.favorites.concat(data.bookmark?.id!),
      },
    });
  }
};

const removeFavorite: MutationUpdaterFn<IRemoveBookmarkEntryMutation> = (
  cache,
  { data },
) => {
  const bookmarks = cache.readQuery<IGetAllBookmarksQuery>({
    query: GetAllBookmarksDocument,
  })!;

  cache.writeQuery<IGetAllBookmarksQuery>({
    query: GetAllBookmarksDocument,
    data: {
      bookmarks: bookmarks.bookmarks.filter(
        (b) => b.id !== data?.removeBookmark?.id,
      ),
      favorites: bookmarks.favorites.filter(
        (id) => data?.removeBookmark?.id !== id,
      ),
    },
  });
};

export const useBookmark = (id?: number) => {
  const client = useApolloClient();
  const [mutationFn, mutationSteate] = useBookmarkEntryMutation({
    update: updateFavorite,
  });
  const [
    removeMutationFn,
    removeMutationState,
  ] = useRemoveBookmarkEntryMutation({
    update: removeFavorite,
  });
  const isBookmarked = useMemo(() => {
    const cache = client.readQuery<IGetAllBookmarksQuery>({
      query: GetAllBookmarksDocument,
    });

    const isFavorite = id ? cache?.favorites?.includes(id) : false;

    return isFavorite;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, client, removeMutationState.loading, mutationSteate.loading]);

  const onBookmark = useCallback(async () => {
    if (id && !isBookmarked) {
      await mutationFn({ variables: { id } });
    } else if (id) {
      await removeMutationFn({ variables: { id } });
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, isBookmarked]);

  return [isBookmarked, onBookmark] as const;
};
