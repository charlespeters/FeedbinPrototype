import { useCallback } from 'react';
import { useNavigation } from '@react-navigation/native';
import { SubscriptionDetailsNavigationProp } from '../routing';

export const usePressFeedItem = (title: string, id: number) => {
  const navigate = useNavigation<SubscriptionDetailsNavigationProp>();

  return useCallback(
    () =>
      navigate.navigate('SubscriptionDetails', {
        title,
        id,
      }),
    [title, id, navigate],
  );
};
