import { useMemo } from 'react';
import { useReducedFeeds } from './useReducedFeeds';

export const useFeedInfo = (id?: number) => {
  const reducedFeeds = useReducedFeeds();

  return useMemo(() => {
    const feed = reducedFeeds!.find((f) => f?.feed_id === id);
    return feed;
  }, [id, reducedFeeds]);
};
