import { useMemo } from 'react';
import { useApolloClient, MutationUpdaterFn } from '@apollo/client';
import {
  IMarkAsReadMutation,
  IMetaFragment,
  MetaFragmentDoc,
} from '../generated';
import { useIsFocused } from '@react-navigation/native';

export const updateUnreadList: MutationUpdaterFn<IMarkAsReadMutation> = (
  cache,
  { data },
) => {
  if (data?.markAsRead?.id) {
    const fragment = cache.readFragment<IMetaFragment>({
      fragment: MetaFragmentDoc,
    })!;

    const s = new Set(fragment.unread);

    if (s.has(data.markAsRead.id)) {
      s.delete(data.markAsRead.id);

      cache.modify({
        fields: {
          unread() {
            const unread = Array.from(s);
            cache.writeFragment<IMetaFragment>({
              fragment: MetaFragmentDoc,
              data: {
                ...fragment,
                unread,
              },
            });

            return unread;
          },
        },
        optimistic: true,
        broadcast: true,
      });
    }
  }
};

export const useUnread = (id: number): boolean => {
  const client = useApolloClient();
  const isFocused = useIsFocused();

  return useMemo(() => {
    const cache = client.readFragment<IMetaFragment>({
      fragment: MetaFragmentDoc,
    });
    const isUnread = cache?.unread.includes(id);

    return !!isUnread;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [id, client.cache, isFocused, client.readFragment]);
};
