import { useSelector, useDispatch } from 'react-redux';
import { IRootState } from '../store';
import * as actions from '../store/actions';
import { ISettings } from '../store/reducers';

export const useDismissBanner = () => {
  const settings = useSelector<IRootState, ISettings>(
    (state) => state.settings,
  );
  const dispatch = useDispatch();
  return [
    settings.bannerDismissed,
    () => dispatch(actions.dismissBanner(true)),
  ] as const;
};
