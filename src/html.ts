import { HTMLNode, HTMLTagNode, HTMLTextNode } from 'react-native-render-html';
import { Falsy } from 'react-native';

const isTextNode = (n: HTMLNode): n is HTMLTextNode => {
  return n.type === 'text';
};

const isTagNode = (n: HTMLNode): n is HTMLTagNode => {
  return n.type === 'tag';
};

export const alterHTMLData = (node: HTMLNode): string | Falsy => {
  if (isTextNode(node)) {
    let { parent, data } = node;
    if (parent && isTagNode(parent)) {
      switch (parent.name) {
        case 'h1':
        case 'h2':
        case 'h3':
        case 'h4':
        case 'h5':
        case 'h6':
        case 'p':
          return data.trimStart();
      }
    }
  }
};

export const getSource = (s: string | undefined) => {
  return {
    html: s || '',
  };
};

export const computeEmbeddedMaxWidth = (
  contentWidth: number,
  _tagName: string,
) => {
  return Math.min(contentWidth, 500);
};
