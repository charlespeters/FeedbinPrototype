import * as React from 'react';
import { FAB } from 'react-native-paper';

export const ListViewWrapper: React.FC = (props) => {
  const [state, setState] = React.useState({ open: false });

  return (
    <>
      {props.children}
      <FAB.Group
        open={state.open}
        visible
        icon="filter-variant"
        actions={[
          {
            label: 'Unread',
            icon: 'view-agenda',
            onPress: () => console.log('Pressed add'),
          },
        ]}
        onStateChange={({ open }) => setState({ open })}
        onPress={() => {
          if (state.open) {
            // do something if the speed dial is open
          }
        }}
      />
    </>
  );
};
