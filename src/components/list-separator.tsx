import * as React from 'react';
import { StyleSheet, View } from 'react-native';
import { Colors } from 'react-native-paper';

export const ListSeparator = () => {
  return <View style={styles.main} />;
};

const styles = StyleSheet.create({
  main: {
    height: 0.5,
    opacity: 0.25,
    backgroundColor: Colors.grey400,
  },
});
