import * as React from 'react';
import { Colors, useTheme } from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

interface IShareButtonProps {
  size?: number;
  onPress: () => void;
}

export const ShareButton: React.FC<IShareButtonProps> = (props) => {
  const theme = useTheme();
  const size = props.size || 24;
  const color = theme.dark ? Colors.white : Colors.grey900;
  return (
    <MaterialCommunityIcons
      name="share-outline"
      size={size}
      style={{ marginHorizontal: 11, marginVertical: 3 }}
      color={color}
      onPress={props.onPress}
    />
  );
};
