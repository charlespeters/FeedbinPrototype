import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Colors } from 'react-native-paper';

interface IFeedListHeaderProps {
  title: string;
}

export const FeedListHeader: React.FC<IFeedListHeaderProps> = (props) => {
  return (
    <View style={styles.headerContainer}>
      <Text>{props.title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: Colors.red500,
  },
  title: {},
});
