import * as React from 'react';
import { Banner } from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { useDismissBanner } from '../hooks';

export const AlphaBanner = () => {
  const [isDismissed, onDismiss] = useDismissBanner();
  return (
    <Banner
      visible={!isDismissed}
      actions={[
        {
          label: 'Got it',
          onPress: onDismiss,
        },
      ]}
      icon={({ size, color }) => (
        <MaterialCommunityIcons name="bug" color={color} size={size} />
      )}>
      Hey there! This is alpha software, so there's bound be things rough around
      the edges. Just remember, swim at your own risk. I'm glad you gave this
      app a spin!
    </Banner>
  );
};
