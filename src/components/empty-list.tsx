import * as React from 'react';
import { View } from 'react-native';
import { Headline } from 'react-native-paper';

export const EmptyList = () => {
  return (
    <View>
      <Headline>Nothing here.</Headline>
    </View>
  );
};
