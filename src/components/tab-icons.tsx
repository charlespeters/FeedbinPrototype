import * as React from 'react';
import { BottomTabNavigationOptions } from '@react-navigation/bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export const tabOptions: Record<string, BottomTabNavigationOptions> = {
  unread: {
    tabBarIcon: ({ color, size }) => (
      <MaterialCommunityIcons name="home" color={color} size={size} />
    ),
  },
  feed: {
    tabBarIcon: ({ color, size }) => (
      <MaterialCommunityIcons
        name="newspaper-variant-multiple"
        color={color}
        size={size}
      />
    ),
  },
  bookmarks: {
    tabBarIcon: ({ color, size }) => (
      <MaterialCommunityIcons
        name="bookmark-multiple"
        color={color}
        size={size}
      />
    ),
  },
  recommend: {
    tabBarIcon: ({ color, size }) => (
      <MaterialCommunityIcons name="gift" color={color} size={size} />
    ),
  },
  settings: {
    tabBarIcon: ({ color, size }) => (
      <MaterialCommunityIcons name="tune" color={color} size={size} />
    ),
  },
};
