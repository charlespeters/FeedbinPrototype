import * as React from 'react';
import { StatusBar } from 'react-native';
import { useTheme } from 'react-native-paper';

export const Status = () => {
  const material = useTheme();
  return (
    <StatusBar
      backgroundColor={material.colors.surface}
      barStyle={material.dark ? 'light-content' : 'dark-content'}
    />
  );
};
