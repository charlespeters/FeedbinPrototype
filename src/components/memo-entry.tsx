import * as React from 'react';
import { ListRenderItem } from 'react-native';
import { IItem } from '../generated';
import { EntryListItem } from './entry';

type ListRenderEntryItem = ListRenderItem<
  {
    __typename?: 'Item' | undefined;
  } & Pick<IItem, 'summary' | 'title' | 'id' | 'feed_id' | 'published'>
>;

export const RenderEntryItem: ListRenderEntryItem = ({ item }) => {
  return (
    <EntryListItem
      id={item.id.toString()}
      title={item.title}
      key={item.id}
      summary={item.summary}
      published={item.published}
      feed_id={item.feed_id}
    />
  );
};

const memoRenderItem = React.memo(RenderEntryItem);

export const MemoEntryItem: ListRenderEntryItem = (props) =>
  React.createElement(memoRenderItem, props);
