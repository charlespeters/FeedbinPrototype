import * as React from 'react';
import { View } from 'react-native';
import { useTheme, ActivityIndicator, Title } from 'react-native-paper';

interface ILoadingPageProps {
  message: string;
}

export const LoadingPage: React.FC<ILoadingPageProps> = (props) => {
  const theme = useTheme();
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: theme.colors.background,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <ActivityIndicator animating color={theme.colors.accent} />
      <Title>{props.message}</Title>
    </View>
  );
};
