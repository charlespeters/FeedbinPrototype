import * as React from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import format from 'date-fns/format';
import formatDistanceToNow from 'date-fns/formatDistanceToNow';
import { Paragraph, Caption, useTheme, Subheading } from 'react-native-paper';
import { ArticleScreenNavigationProp } from '../routing';
import { useFeedInfo, useUnread } from '../hooks';

interface IEntryListItemProps {
  id: string;
  title: string;
  summary: string | null;
  published: string;
  feed_id: number;
}

export const EntryListItem: React.FC<IEntryListItemProps> = (props) => {
  const id = parseInt(props.id, 10);
  const d = new Date(props.published);
  const { colors } = useTheme();

  const isUnread = useUnread(id);
  const navigate = useNavigation<ArticleScreenNavigationProp>();
  const date = formatDistanceToNow(d, {
    addSuffix: true,
  });

  const formatedDate = format(d, 'dd LLL yyyy');

  const feed = useFeedInfo(props.feed_id);

  const onPress = React.useCallback(
    () =>
      navigate.navigate('Article', {
        title: props.title,
        id,
      }),
    [navigate, props, id],
  );

  const outerStyles = React.useMemo(
    () => [
      styles.container,
      { backgroundColor: colors.background },
      isUnread && styles.unreadContainer,
    ],
    [colors, isUnread],
  );

  return (
    <TouchableOpacity style={outerStyles} onPress={onPress}>
      <View style={styles.inner}>
        <View style={styles.headerDetails}>
          <Caption>{feed?.name}</Caption>
        </View>
        <View style={styles.titleContainer}>
          <Subheading>{props.title}</Subheading>
        </View>

        {props.summary !== null && (
          <View>
            <Paragraph>{props.summary}</Paragraph>
          </View>
        )}
        <View>
          <Caption style={styles.meta}>
            {date} &bull; {formatedDate}
          </Caption>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  headerDetails: {
    marginBottom: 4,
  },
  container: {
    flexDirection: 'row',
    paddingVertical: 8,
    paddingHorizontal: 16,
    alignItems: 'flex-start',
    opacity: 0.5,
  },
  unreadContainer: {
    opacity: 1,
  },
  meta: { fontFamily: 'IBMPlexSans-Italic' },
  inner: { width: 0, flexGrow: 1 },
  titleContainer: { flexDirection: 'row' },
});
