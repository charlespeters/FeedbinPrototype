import * as React from 'react';
import {
  View,
  Text,
  Dimensions,
  Linking,
  GestureResponderEvent,
  Falsy,
  Platform,
} from 'react-native';
import { withTheme } from 'react-native-paper';
import color from 'color';
import isEqual from 'react-fast-compare';
import HTML, {
  HTMLNode,
  HTMLTagNode,
  HTMLTextNode,
  StylesDictionary,
  RendererDictionary,
  PassProps,
} from 'react-native-render-html';
import WebView from 'react-native-webview';
import iframe from '@native-html/iframe-plugin';

const isTextNode = (n: HTMLNode): n is HTMLTextNode => {
  return n.type === 'text';
};

const isTagNode = (n: HTMLNode): n is HTMLTagNode => {
  return n.type === 'tag';
};

export const alterHTMLData = (node: HTMLNode): string | Falsy => {
  if (isTextNode(node)) {
    if (node.parent && isTagNode(node.parent)) {
      switch (node.parent.name) {
        case 'h1':
        case 'h2':
        case 'h3':
        case 'h4':
        case 'h5':
        case 'h6':
          return node.data.trimStart();
      }
    }
  }
};

export const getSource = (s: string | undefined) => {
  return {
    html: s || '',
  };
};

export const getTextProps = (
  passProps: PassProps<any> & { selectable?: boolean },
) => {
  const { selectable, allowFontScaling, defaultTextProps } = passProps;
  return {
    selectable,
    allowFontScaling,
    ...defaultTextProps,
  };
};

export const computeEmbeddedMaxWidth = (
  contentWidth: number,
  _tagName: string,
) => {
  return Math.min(contentWidth, 500);
};

export const _generateHeadingStyle = (
  baseFontSize: number,
  fontMultiplier: number,
  marginMultiplier: number,
) => {
  return {
    fontSize: baseFontSize * fontMultiplier,
    marginTop: baseFontSize * fontMultiplier * marginMultiplier,
    marginBottom: baseFontSize * fontMultiplier * marginMultiplier,
    fontWeight: 'bold',
  };
};

interface ICustomHTMLProps {
  content?: string;
  theme: ReactNativePaper.Theme;
}

export class _CustomHTML extends React.Component<ICustomHTMLProps> {
  private _list: RendererDictionary<any>;
  private _tagStyles: StylesDictionary;
  constructor(props: ICustomHTMLProps) {
    super(props);

    this._tagStyles = this._generateStyles(props.theme, 14);
    this._list = this._createListRenderer(props.theme, 14);
  }

  private async _handleLinkPress(_event: GestureResponderEvent, href: string) {
    await Linking.openURL(href);
  }

  private _renderers = {
    iframe,
  };

  private _source = {
    html: this.props.content || '',
  };
  private _contentWidth = Dimensions.get('window').width;

  private _fontStyling = {
    color: this.props.theme.colors.text,
    ...this.props.theme.fonts.regular,
  };

  public _generateStyles = (
    theme: ReactNativePaper.Theme,
    baseFontSize: number,
  ): StylesDictionary => {
    return {
      a: {
        fontSize: baseFontSize,
        textDecorationLine: 'none',
        color: theme.colors.primary,
      },
      h1: _generateHeadingStyle(baseFontSize, 2, 0.67),
      h2: _generateHeadingStyle(baseFontSize, 1.5, 0.83),
      h3: _generateHeadingStyle(baseFontSize, 1.17, 1),
      h4: _generateHeadingStyle(baseFontSize, 1, 1.33),
      h5: _generateHeadingStyle(baseFontSize, 0.83, 1.67),
      h6: _generateHeadingStyle(baseFontSize, 0.67, 2.33),
      figcaption: {
        fontSize: baseFontSize * 0.75,
        color: color(theme.colors.text).alpha(0.75).rgb().string(),
        marginTop: baseFontSize / 2,
        padding: 8,
      },
      pre: {
        fontFamily:
          Platform.OS === 'ios' ? 'IBM Plex Mono' : 'IBMPlexMono-Regular',
        fontWeight: '400',
      },
      code: {
        fontFamily:
          Platform.OS === 'ios' ? 'IBM Plex Mono' : 'IBMPlexMono-Regular',
        fontWeight: '400',
      },
    };
  };

  public _createListRenderer = (
    theme: ReactNativePaper.Theme,
    baseFontSize: number,
  ) => {
    const styles = {
      marginRight: 10,
      width: baseFontSize / 2.8,
      height: baseFontSize / 2.8,
      marginTop: baseFontSize / 2,
      borderRadius: baseFontSize / 2.8,
      backgroundColor: theme.colors.text,
    };
    return {
      ul: () => {
        return <View style={styles} />;
      },
      ol: (_: any, __: any, ___: any, { index, ...passProps }: any) => {
        const textProps = getTextProps(passProps);
        const style = {
          marginRight: 5,
          color: theme.colors.text,
          fontSize: baseFontSize,
        };
        return (
          <Text {...textProps} style={style}>
            {index + 1}.
          </Text>
        );
      },
    };
  };

  public shouldComponentUpdate(nextProps: ICustomHTMLProps) {
    return !isEqual(this.props, nextProps);
  }

  public render() {
    return (
      <HTML
        renderers={this._renderers}
        source={this._source}
        emSize={16}
        WebView={WebView}
        onLinkPress={this._handleLinkPress}
        contentWidth={this._contentWidth}
        alterData={alterHTMLData}
        computeEmbeddedMaxWidth={computeEmbeddedMaxWidth}
        baseFontStyle={this._fontStyling}
        listsPrefixesRenderers={this._list}
        tagsStyles={this._tagStyles}
      />
    );
  }
}

export const CustomHTMLRender = withTheme(_CustomHTML);
