import * as React from 'react';
import { List } from 'react-native-paper';
import { usePressFeedItem } from '../hooks';

interface IFeedItem {
  title: string;
  id: number;
}

export const FeedItem: React.FC<IFeedItem> = (props) => {
  const onPress = usePressFeedItem(props.title, props.id);
  return <List.Item title={props.title} onPress={onPress} />;
};
