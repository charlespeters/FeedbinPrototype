import {
  createStackNavigator,
  StackNavigationProp,
} from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

export type RootStackParamList = {
  Login: undefined;
  Main: undefined;
  Settings: undefined;
};

interface IArticleParams {
  title: string;
  id: number;
}

export type TabUIParamList = {
  Feed: undefined;
  Unread: undefined;
  Article: IArticleParams;
  Bookmarks: undefined;
  Recommended: undefined;
  Settings: undefined;
};

export const TabsUI = createBottomTabNavigator<TabUIParamList>();

export const RootStack = createStackNavigator<RootStackParamList>();

export type ArticleScreenRouteProp = RouteProp<TabUIParamList, 'Article'>;

export type ArticleScreenNavigationProp = StackNavigationProp<
  TabUIParamList,
  'Article'
>;

interface ISubscriptParams {
  title: string;
  id: number;
}

export type FeedParamList = {
  Subscription: undefined;
  SubscriptionDetails: ISubscriptParams;
  Article: IArticleParams;
};

export type SubscriptionDetailsNavigationProp = StackNavigationProp<
  FeedParamList,
  'SubscriptionDetails'
>;
export type SubscriptionDetailsRouteProp = RouteProp<
  FeedParamList,
  'SubscriptionDetails'
>;
