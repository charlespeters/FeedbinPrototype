import {
  DefaultTheme,
  DarkTheme,
  configureFonts,
  Colors,
} from 'react-native-paper';
import {
  DarkTheme as NavigationDarkTheme,
  DefaultTheme as NavigationDefaultTheme,
} from '@react-navigation/native';
import merge from 'deepmerge';

const fontConfig = {
  ios: {
    regular: {
      fontFamily: 'IBM Plex Sans',
      fontWeight: '400' as any,
    },
    medium: {
      fontFamily: 'IBM Plex Sans Medium',
      fontWeight: '500' as any,
    },
    light: {
      fontFamily: 'IBM Plex Sans Light',
      fontWeight: '300' as any,
    },
    thin: {
      fontFamily: 'IBM Plex Sans Thin',
      fontWeight: '200' as any,
    },
  },
  default: {
    regular: {
      fontFamily: 'IBMPlexSans-Regular',
      fontWeight: '400' as any,
    },
    medium: {
      fontFamily: 'IBMPlexSans-Medium',
      fontWeight: '500' as any,
    },
    light: {
      fontFamily: 'IBMPlexSans-Light',
      fontWeight: '300' as any,
    },
    thin: {
      fontFamily: 'IBMPlexSans-Thin',
      fontWeight: '200' as any,
    },
  },
};

export const darkTheme = merge<any>(NavigationDarkTheme, {
  ...DarkTheme,
  dark: true,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    surface: Colors.grey800,
    text: Colors.white,
    background: Colors.grey900,
    primary: Colors.amber500,
    accent: Colors.red400,
    placeholder: Colors.grey200,
  },
  fonts: configureFonts(fontConfig),
});

export const lightTheme = merge<any>(NavigationDefaultTheme, {
  ...DefaultTheme,
  dark: false,
  roundness: 2,
  fonts: configureFonts(fontConfig),
  colors: {
    ...DefaultTheme.colors,
    primary: Colors.amber500,
    accent: Colors.red400,
  },
});
