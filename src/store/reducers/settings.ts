import { Appearance } from 'react-native';
import { Reducer } from 'redux';
import { SettingsAction, SettingsActionTypes } from '../actions';

export interface ISettings {
  theme: 'a' | 'b' | 'c';
  useSystemDefault: boolean;
  isDarkMode: boolean;
  bannerDismissed: boolean;
}

const initialSettings: ISettings = {
  isDarkMode: false,
  useSystemDefault: true,
  theme: 'a',
  bannerDismissed: false,
};

export const settingsReducer: Reducer<ISettings, SettingsAction> = (
  state = initialSettings,
  action,
) => {
  switch (action.type) {
    case SettingsActionTypes.SET_USE_THEME:
      return {
        ...state,
        useSystemDefault: action.useSystem,
        isDarkMode: Appearance.getColorScheme() === 'dark',
      };
    case SettingsActionTypes.SET_THEME:
      return { ...state, theme: action.theme };
    case SettingsActionTypes.SET_DARK_MODE:
      return { ...state, isDarkMode: action.darkMode };
    case SettingsActionTypes.TOGGLE_INFO_BANNER:
      return { ...state, bannerDismissed: action.isDismissed };
    default:
      return state;
  }
};
