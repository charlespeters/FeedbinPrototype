import { Reducer } from 'redux';
import { UserAction, UserActionTypes } from '../actions';

export interface IUser {
  token: string | null;
  isAuthenticated: boolean;
  isInitialized: boolean;
}

const initialUser: IUser = {
  token: null,
  isAuthenticated: false,
  isInitialized: false,
};

export const userReducer: Reducer<IUser, UserAction> = (
  state = initialUser,
  action,
) => {
  switch (action.type) {
    case UserActionTypes.LOGIN:
      return { ...state, token: action.token, isAuthenticated: true };
    case UserActionTypes.LOGOUT:
      return { ...state, token: null, isAuthenticated: false };
    case UserActionTypes.CHECK:
      return {
        ...state,
        token: action.token,
        isAuthenticated: !!action.token,
        isInitialized: true,
      };
    default:
      return state;
  }
};
