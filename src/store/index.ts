import AsyncStorage from '@react-native-async-storage/async-storage';
import { applyMiddleware, combineReducers, createStore } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';

import { composeWithDevTools } from 'redux-devtools-extension';

import thunk from 'redux-thunk';

import * as reducer from './reducers';
import { STORE_KEY } from '../utils';

export interface IRootState {
  user: reducer.IUser;
  settings: reducer.ISettings;
}

export const rootReducer = combineReducers<IRootState>({
  user: reducer.userReducer,
  settings: reducer.settingsReducer,
});

const persistConfig = {
  key: STORE_KEY,
  storage: AsyncStorage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(
  persistedReducer,
  composeWithDevTools(applyMiddleware(thunk)),
);

export const persistor = persistStore(store);
