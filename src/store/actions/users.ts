import { ThunkAction } from 'redux-thunk';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { FEEDBIN_KEY } from '../../utils';
import { IRootState } from '..';

export enum UserActionTypes {
  LOGIN = 'LOGIN',
  LOGOUT = 'LOGOUT',
  CHECK = 'CHECK_TOKEN',
}

export interface IUserLogin {
  type: UserActionTypes.LOGIN;
  token: string;
}

export interface IUserLogout {
  type: UserActionTypes.LOGOUT;
}

export interface IUserCheckToken {
  type: UserActionTypes.CHECK;
  token: string | null;
}

export type UserAction = IUserLogin | IUserLogout | IUserCheckToken;

export const login = (token: string): IUserLogin => ({
  type: UserActionTypes.LOGIN,
  token,
});

const getToken = async () => await AsyncStorage.getItem(FEEDBIN_KEY);
const removeToken = async () => await AsyncStorage.removeItem(FEEDBIN_KEY);

export const loginAsync = (
  value: string,
): ThunkAction<Promise<void>, IRootState, string, IUserLogin> => async (
  dispatch,
) => {
  await AsyncStorage.setItem(FEEDBIN_KEY, value);

  if (value) {
    const action = login(value);
    dispatch(action);
  }
};

export const checkTokenAsync = (): ThunkAction<
  Promise<void>,
  IRootState,
  any,
  IUserCheckToken
> => async (dispatch) => {
  const token = await getToken();

  const action = checkToken(token);

  dispatch(action);
};

export const logout = (): IUserLogout => ({ type: UserActionTypes.LOGOUT });

export const logoutAsync = (): ThunkAction<
  Promise<void>,
  IRootState,
  void,
  IUserLogout
> => async (dispatch) => {
  await removeToken();

  dispatch(logout());
};

export const checkToken = (token: string | null): IUserCheckToken => ({
  type: UserActionTypes.CHECK,
  token,
});
