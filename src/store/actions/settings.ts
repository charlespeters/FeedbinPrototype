export enum SettingsActionTypes {
  SET_THEME = 'SET_THEME',
  SET_DARK_MODE = 'SET_DARK_MODE',
  TOGGLE_INFO_BANNER = 'TOGGLE_INFO_BANNER',
  SET_USE_THEME = 'SET_USE_THEME',
}

interface ISettingsSetTheme {
  type: SettingsActionTypes.SET_THEME;
  theme: 'a' | 'b' | 'c';
}

interface ISettingsDismissBanner {
  type: SettingsActionTypes.TOGGLE_INFO_BANNER;
  isDismissed: boolean;
}

interface ISettingsSetDarkMode {
  type: SettingsActionTypes.SET_DARK_MODE;
  darkMode: boolean;
}

interface ISettingsUseSystemTheme {
  type: SettingsActionTypes.SET_USE_THEME;
  useSystem: boolean;
}

export type SettingsAction =
  | ISettingsSetTheme
  | ISettingsSetDarkMode
  | ISettingsDismissBanner
  | ISettingsUseSystemTheme;

export const setTheme = (theme: 'a' | 'b' | 'c'): ISettingsSetTheme => ({
  type: SettingsActionTypes.SET_THEME,
  theme,
});

export const setUseSystemTheme = (useSystem: boolean) => ({
  type: SettingsActionTypes.SET_USE_THEME,
  useSystem,
});

export const dismissBanner = (
  isDismissed: boolean,
): ISettingsDismissBanner => ({
  type: SettingsActionTypes.TOGGLE_INFO_BANNER,
  isDismissed,
});

export const setDarkMode = (darkMode: boolean): ISettingsSetDarkMode => ({
  type: SettingsActionTypes.SET_DARK_MODE,
  darkMode,
});
