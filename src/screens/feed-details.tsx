import * as React from 'react';
import { View, RefreshControl, FlatList, StyleSheet } from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { Text, Colors, FAB, useTheme } from 'react-native-paper';
import { useFeedwithEntriesQuery } from '../generated';
import { LoadingPage, ListSeparator, MemoEntryItem } from '../components';
import {
  SubscriptionDetailsRouteProp,
  SubscriptionDetailsNavigationProp,
} from '../routing';
import { itemKeyExtractor } from '../utils';

export const FeedDetails = () => {
  const { colors } = useTheme();
  const route = useRoute<SubscriptionDetailsRouteProp>();
  const navigation = useNavigation<SubscriptionDetailsNavigationProp>();

  const { loading, data, error, refetch } = useFeedwithEntriesQuery({
    variables: {
      id: route.params.id,
    },
  });

  const [refreshing, setRefreshing] = React.useState(false);
  const [value] = React.useState(route.params.title);
  React.useLayoutEffect(() => {
    navigation.setOptions({
      title: value === '' ? 'No title' : value,
    });
  }, [navigation, value]);
  const onRefresh = React.useCallback(async () => {
    setRefreshing(true);

    try {
      await refetch().then(() => setRefreshing(false));
    } catch (err) {
      console.log(err);
    }
  }, [refetch, setRefreshing]);

  return (
    <>
      <View style={{ backgroundColor: colors.background }}>
        {error && (
          <View style={styles.errorContainer}>
            <Text style={styles.errorText}>{error.message}</Text>
          </View>
        )}
        {loading ? (
          <LoadingPage message="Fetching Your Feeds" />
        ) : (
          !!data &&
          !loading && (
            <FlatList
              refreshControl={
                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
              }
              data={data.subscription.items}
              keyExtractor={itemKeyExtractor}
              ItemSeparatorComponent={ListSeparator}
              renderItem={MemoEntryItem}
            />
          )
        )}
      </View>
      <FAB
        icon="pencil"
        style={styles.fab}
        onPress={() => console.log('Press')}
      />
    </>
  );
};

const styles = StyleSheet.create({
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
  errorContainer: { flex: 1, backgroundColor: Colors.grey900 },
  errorText: { color: Colors.white },
});
