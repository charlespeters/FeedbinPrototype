import * as React from 'react';
import { View, RefreshControl, FlatList } from 'react-native';
import { Text, useTheme } from 'react-native-paper';
import { LoadingPage, MemoEntryItem, ListSeparator } from '../components';
import { useGetAllBookmarksQuery } from '../generated';
import { itemKeyExtractor } from '../utils';

export const BookmarksScreen = () => {
  const { colors } = useTheme();
  const { loading, data, error, refetch } = useGetAllBookmarksQuery();

  const [refreshing, setRefresh] = React.useState<boolean>(false);

  const onRefresh = React.useCallback(async () => {
    setRefresh(true);
    await refetch().then(() => setRefresh(false));
  }, [refetch, setRefresh]);

  return (
    <View style={{ backgroundColor: colors.background }}>
      {error && (
        <View style={{ flex: 1, backgroundColor: colors.background }}>
          <Text style={{ color: colors.text }}>{error.message}</Text>
        </View>
      )}
      {loading ? (
        <LoadingPage message="Fetching your articles" />
      ) : (
        !!data &&
        !loading && (
          <FlatList
            refreshControl={
              <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
            }
            data={data.bookmarks}
            keyExtractor={itemKeyExtractor}
            ItemSeparatorComponent={ListSeparator}
            renderItem={MemoEntryItem}
          />
        )
      )}
    </View>
  );
};
