import * as React from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  Linking,
  Share,
  ShareContent,
  TouchableWithoutFeedback,
} from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import {
  Caption,
  Headline,
  FAB,
  useTheme,
  Text,
  Divider,
} from 'react-native-paper';
import format from 'date-fns/format';
import { LoadingPage, ShareButton, CustomHTMLRender } from '../components';
import {
  AllUnreadDocument,
  useEntryDocumentQuery,
  useMarkAsReadMutation,
} from '../generated';
import {
  ArticleScreenNavigationProp,
  ArticleScreenRouteProp,
} from '../routing';
import { useBookmark, useFeedInfo } from '../hooks';

// const baseFontSize = 14;

export const useGenerateStyles = () => {
  const theme = useTheme();
  return React.useCallback(
    (
      baseFontSize: number,
      fontMultiplier: number,
      marginMultiplier: number,
      fontFamily: any,
    ) => {
      return {
        color: theme.colors.primary,
        fontSize: baseFontSize * fontMultiplier,
        marginTop: baseFontSize * fontMultiplier * marginMultiplier,
        marginBottom: baseFontSize * fontMultiplier * marginMultiplier,
        ...fontFamily,
      };
    },
    [theme],
  );
};

export const ArticleScreen = () => {
  const theme = useTheme();

  const route = useRoute<ArticleScreenRouteProp>();
  const navigation = useNavigation<ArticleScreenNavigationProp>();
  const [isBookmarked, onBookmark] = useBookmark(route.params.id);

  const [markAsReadFn] = useMarkAsReadMutation({
    refetchQueries: [{ query: AllUnreadDocument }],
    awaitRefetchQueries: true,
  });
  const entryDoc = useEntryDocumentQuery({
    variables: {
      id: route.params.id,
    },
  });

  const feed = useFeedInfo(entryDoc.data?.entry.feed_id);

  const [value] = React.useState(route.params.title);

  React.useLayoutEffect(() => {
    const getShareContent = (): ShareContent => {
      const content = {
        title: value,
        url: entryDoc.data?.entry.url!,
        message: entryDoc.data?.entry.url!,
      };
      return content;
    };
    navigation.setOptions({
      title: value === '' ? 'No title' : value,
      headerRight: () => (
        <ShareButton
          onPress={() =>
            Share.share(getShareContent(), { dialogTitle: 'Share from Reubin' })
          }
        />
      ),
    });
  }, [navigation, value, entryDoc.data]);

  React.useEffect(() => {
    if (route.params.id) {
      markAsReadFn({
        variables: {
          id: route.params.id.toString(),
        },
        // update: updateUnreadList,
      });
    }
  }, [markAsReadFn, route.params.id]);

  return (
    <>
      <ScrollView style={styles.scrollContainer}>
        <View style={styles.contentContainer}>
          <View style={styles.header}>
            {!!feed && <Caption>{feed.name}</Caption>}
            <Headline>{value}</Headline>
            {!entryDoc.loading && !!entryDoc.data && (
              <Caption>
                {format(
                  new Date(entryDoc.data.entry.published),
                  'dd LLLL yyyy',
                )}
              </Caption>
            )}
          </View>

          {entryDoc.loading ? (
            <LoadingPage message="Need the post" />
          ) : (
            !entryDoc.loading &&
            !!entryDoc.data && (
              <View style={styles.htmlContainer}>
                <CustomHTMLRender content={entryDoc.data?.entry.content} />
                <View style={styles.PermalinkContainer}>
                  <Divider style={styles.divider} />
                  <TouchableWithoutFeedback
                    onPress={() => {
                      if (entryDoc.data?.entry?.url) {
                        Linking.openURL(entryDoc.data.entry.url);
                      }
                    }}>
                    <Text style={{ color: theme.colors.accent }}>
                      Permalink
                    </Text>
                  </TouchableWithoutFeedback>
                </View>
              </View>
            )
          )}
        </View>
      </ScrollView>
      <FAB
        icon={isBookmarked ? 'bookmark' : 'bookmark-outline'}
        visible
        style={styles.fab}
        onPress={onBookmark}
      />
    </>
  );
};

const styles = StyleSheet.create({
  header: {
    paddingTop: 64,
    marginBottom: 16,
    flex: 1,
  },
  PermalinkContainer: {
    marginTop: 8,
    paddingTop: 8,
  },
  permalink: {},
  contentContainer: {
    paddingHorizontal: 4,
    flex: 1,
  },
  htmlContainer: {
    flex: 1,
    marginBottom: 64,
  },
  scrollContainer: {
    flex: 1,
  },
  fab: {
    position: 'absolute',
    margin: 16,
    right: 0,
    bottom: 0,
  },
  divider: { marginBottom: 4 },
});
