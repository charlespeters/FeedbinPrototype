import * as React from 'react';
import { StyleSheet, View, ScrollView, RefreshControl } from 'react-native';
import { List, Snackbar } from 'react-native-paper';
import { LoadingPage, FeedItem } from '../components';
import { useAllSubscriptionsQuery } from '../generated';

export const FeedListScreen = () => {
  const { loading, data, error, refetch } = useAllSubscriptionsQuery();
  const [refreshing, setRefreshing] = React.useState(false);
  const onDismissSnackBar = () => {};

  const onRefresh = React.useCallback(() => {
    setRefreshing(true);

    refetch().then(() => setRefreshing(false));
  }, [refetch]);

  return (
    <ScrollView
      style={styles.container}
      refreshControl={
        <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
      }>
      {loading ? (
        <LoadingPage message="Fetching Your Subscriptions" />
      ) : (
        !!data && (
          <>
            <View>
              <List.Subheader>Tags</List.Subheader>
              {data?.subscriptions.tags.map((tag, i) => {
                return (
                  <List.Accordion title={tag.title} id={i} key={i}>
                    {tag.feeds.map((feed, i) => {
                      return (
                        <FeedItem
                          title={feed?.name!}
                          id={feed?.feed_id!}
                          key={i}
                        />
                      );
                    })}
                  </List.Accordion>
                );
              })}
            </View>

            <View>
              <List.Subheader>Feeds</List.Subheader>
              {data.subscriptions.untaggedFeeds.map((feed, i) => {
                return (
                  <FeedItem title={feed?.name} id={feed?.feed_id!} key={i} />
                );
              })}
            </View>
          </>
        )
      )}
      <Snackbar
        visible={!!error}
        onDismiss={onDismissSnackBar}
        action={{
          label: 'Retry',
          onPress: () => refetch(),
        }}>
        {error?.message}
      </Snackbar>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
