import * as React from 'react';
import { ScrollView, StyleSheet, View } from 'react-native';
import { Headline, List } from 'react-native-paper';

export const RecommendedScreen = () => {
  return (
    <ScrollView>
      <View style={styles.header}>
        <Headline>Recommended Feeds</Headline>
      </View>
      <List.Section title="News">
        <List.Item title="CNN" />
        <List.Item title="New York Times" />
        <List.Item title="Vox" />
      </List.Section>
      <List.Section title="Tech">
        <List.Item title="The Verge" />
        <List.Item title="Polygon" />
        <List.Item title="Eater" />
      </List.Section>
      <List.Section title="Relevant">
        <List.Item title="Reubin Changelog" />
        <List.Item title="Charlie Peters Blog" />
      </List.Section>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  header: { marginHorizontal: 16, marginVertical: 32 },
});
