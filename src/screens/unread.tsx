import * as React from 'react';
import { View, RefreshControl, FlatList, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { Text, FAB, useTheme } from 'react-native-paper';
import is from '@sindresorhus/is';
import {
  AlphaBanner,
  EmptyList,
  ListSeparator,
  LoadingPage,
  RenderEntryItem,
} from '../components';
import { useAllUnreadQuery } from '../generated';
import { itemKeyExtractor, sortByDate, Filters } from '../utils';

interface IFilterFabProps {
  open: boolean;
  onFilter(f: Filters): void;
  onSetOpen(open: boolean): void;
}

export const FilterFab: React.FC<IFilterFabProps> = (props) => {
  const onStateChange = (value: { open: boolean }) =>
    props.onSetOpen(value.open);

  const actions = [
    {
      label: 'Unread',
      icon: 'view-agenda',
      onPress: () => props.onFilter(Filters.UNREAD),
    },
    {
      label: 'All',
      icon: 'view-agenda',
      onPress: () => props.onFilter(Filters.ALL),
    },
  ];
  return (
    <FAB.Group
      open={props.open}
      visible
      icon="filter-variant"
      actions={actions}
      onStateChange={onStateChange}
      onPress={() => {
        if (props.open) {
          // do something if the speed dial is open
        }
      }}
    />
  );
};

export const UnreadScreen = () => {
  const page = React.useRef<number>(1);

  const [open, setOpen] = React.useState<boolean>(false);
  const [activeFilter, setActiveFilter] = React.useState<Filters>(Filters.ALL);
  const { colors } = useTheme();

  const navigation = useNavigation();
  const { loading, data, error, refetch, fetchMore } = useAllUnreadQuery();

  React.useEffect(() => {
    navigation.setOptions({
      title: activeFilter === Filters.UNREAD ? 'Unread Entries' : 'All Entries',
    });
  }, [navigation, activeFilter]);

  const [refreshing, setRefreshing] = React.useState<boolean>(false);
  const onRefresh = React.useCallback(async () => {
    if (refetch) {
      setRefreshing(true);
      try {
        await refetch().then(() => setRefreshing(false));
      } catch (err) {
        console.log(err);
      }
    }
  }, [refetch, setRefreshing]);

  const onEndReached = React.useCallback(async () => {
    if (fetchMore) {
      try {
        page.current++;
        await fetchMore({
          variables: {
            page: page.current,
          },
        });
      } catch (err) {
        console.log(err);
      }
    }
  }, [fetchMore]);

  const containerStyle = React.useMemo(
    () => [styles.outerContainer, { backgroundColor: colors.background }],
    [colors],
  );

  const entries = React.useMemo(() => {
    if (data) {
      const clone = Array.from(data.entries).sort(sortByDate);
      switch (activeFilter) {
        case Filters.ALL:
          return clone;
        case Filters.UNREAD: {
          const s = new Set(data?.unread);

          return clone.filter((e) => s.has(e.id));
        }
        default:
          return data?.entries;
      }
    }
    return data;
  }, [data, activeFilter]);

  return (
    <>
      <View style={containerStyle}>
        <AlphaBanner />
        {error && (
          <View style={containerStyle}>
            <Text>{error.message}</Text>
          </View>
        )}
        {loading && is.undefined(data) ? (
          <LoadingPage message="Fetching your articles" />
        ) : (
          !!data && (
            <FlatList
              refreshControl={
                <RefreshControl refreshing={refreshing} onRefresh={onRefresh} />
              }
              data={entries}
              initialNumToRender={10}
              keyExtractor={itemKeyExtractor}
              ListEmptyComponent={EmptyList}
              ItemSeparatorComponent={ListSeparator}
              renderItem={RenderEntryItem}
              onEndReachedThreshold={4}
              onEndReached={onEndReached}
            />
          )
        )}
      </View>
      <FilterFab open={open} onSetOpen={setOpen} onFilter={setActiveFilter} />
    </>
  );
};

const styles = StyleSheet.create({
  outerContainer: {
    flex: 1,
  },
});
