import * as React from 'react';
import { ScrollView, StyleSheet } from 'react-native';
import { useAsyncStorage } from '@react-native-async-storage/async-storage';
import { Divider, List, Switch, IconButton } from 'react-native-paper';
import { useDispatch, useSelector } from 'react-redux';
import { IRootState } from '../store';
import * as actions from '../store/actions';
import { ISettings } from '../store/reducers';
import { CACHE_KEY } from '../utils';

export const SettingsScreen = () => {
  const settings = useSelector<IRootState, ISettings>((s) => s.settings);
  const dispatch = useDispatch();
  const cacheStorage = useAsyncStorage(CACHE_KEY);

  return (
    <ScrollView style={styles.container}>
      <List.Section>
        <List.Subheader>Display</List.Subheader>
        <List.Item
          title="Use System Theme"
          right={() => {
            return (
              <Switch
                value={settings.useSystemDefault}
                onValueChange={(v) => dispatch(actions.setUseSystemTheme(v))}
              />
            );
          }}
        />
        <Divider />
        <List.Item
          title="Dark Mode"
          right={() => {
            return (
              <Switch
                disabled={settings.useSystemDefault}
                value={settings.isDarkMode}
                onValueChange={(v) => dispatch(actions.setDarkMode(v))}
              />
            );
          }}
        />
        <Divider />
        <List.Item
          title="Alpha Software Info Banner"
          right={() => {
            return (
              <Switch
                value={!settings.bannerDismissed}
                onValueChange={(v) => dispatch(actions.dismissBanner(!v))}
              />
            );
          }}
        />
        <Divider />
      </List.Section>
      <List.Section title="User">
        <List.Item
          title="Signout"
          right={() => (
            <IconButton
              icon="logout"
              size={16}
              onPress={() => dispatch(actions.logoutAsync())}
            />
          )}
        />
        <Divider />
        <List.Item
          title="Clear Local Cache"
          right={() => (
            <IconButton
              icon="trash-can-outline"
              size={16}
              onPress={() => cacheStorage.removeItem()}
            />
          )}
        />
      </List.Section>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
