import * as React from 'react';
import { View, StyleSheet, ViewStyle, TextStyle } from 'react-native';
import { Headline, Subheading, Button, TextInput } from 'react-native-paper';
import { useConnectedLoginForm } from '../hooks';

export const LoginScreen: React.FC = () => {
  const formik = useConnectedLoginForm();

  return (
    <View style={styles.container}>
      <View style={styles.formContainer}>
        <View style={styles.titleContainer}>
          <Headline>Reubin</Headline>
          <Subheading>A Feedbin Client</Subheading>
        </View>
        <TextInput
          value={formik.values.username}
          autoCompleteType="email"
          label="Username"
          onChangeText={formik.handleChange('username')}
          style={styles.input}
        />
        <TextInput
          value={formik.values.password}
          autoCompleteType="password"
          label="Password"
          secureTextEntry
          onChangeText={formik.handleChange('password')}
          style={styles.input}
        />
        <Button onPress={formik.handleSubmit} mode="contained">
          Submit
        </Button>
      </View>
    </View>
  );
};

interface ILoginStyles {
  container: ViewStyle;
  titleContainer: ViewStyle;
  formContainer: ViewStyle;
  input: TextStyle;
}

const styles = StyleSheet.create<ILoginStyles>({
  container: {
    flex: 1,
    // backgroundColor: Colors.grey900,
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  titleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 32,
  },
  formContainer: {
    width: '100%',
    padding: 8,
  },
  input: {
    marginBottom: 8,
  },
});
